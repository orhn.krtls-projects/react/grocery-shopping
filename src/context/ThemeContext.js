import { createContext, useReducer } from "react";

export const ThemeContext = createContext()

//Reducer function
const themeReducer = (state, action) => {
    switch (action.type) {
        case 'CHANGE_MODE' :
            return { ...state, mode: action.payload }
        default:
            return state
    }
}

export function ThemeProvider({children}) {
    
    //set initial state values
    const [state, dispatch] = useReducer(themeReducer, { mode: 'dark' })
    
    const changeMode = (mode) => {
        //useReducer içindeki reducer function çalıştırılır.
        dispatch({ type: 'CHANGE_MODE', payload: mode })
    }

    return (
        <ThemeContext.Provider value={{ ...state, changeMode }}>
            {children}
        </ThemeContext.Provider>
    )
}