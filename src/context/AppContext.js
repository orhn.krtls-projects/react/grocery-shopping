import React, { Component } from 'react'
export const AppContext = React.createContext()

const reducer = (state, action) => {
    console.log(state)
    console.log(action)
}

class AppContextProvider extends Component {
    constructor(props) {

        super(props);

        this.state = {
            language: 'EN',
            solution: 'JS-React',
            email: "",
            message: ""
        }
    }

    render() {
        const {email,message} = this.state;
        return (
            <AppContext.Provider
                value={
                    {
                        getLanguage: this.state.language,
                        setLanguage: (newLanguage) => this.setState({ language: newLanguage }),
                        getSolution: this.state.solution,
                        setSolution: (newSolution) => this.setSolution({ solution: newSolution })
                    }
                }
            >
                {this.props.children}
            </AppContext.Provider>
        )
    }
}


//const AppContextConsumer = AppContext.Consumer

export default AppContextProvider
//export {AppContextConsumer}