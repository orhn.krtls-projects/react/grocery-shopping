import React, { Component } from 'react';
import Footer from './components/Footer';
import NotFound from './components/NotFound';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Header from './components/Header';
import { useTheme } from './hooks/useTheme';

function App() {

  const { mode } = useTheme

  return (
    <div className='container-fluid'>
      <div className={`App ${mode}`}>
    <Header />
    <BrowserRouter>
      <Routes>
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
    <Footer />
  </div>
  </div>
  );
}

export default App;
