import React from 'react'
import { useTheme } from '../hooks/useTheme'
import './Header.css'
import modeIcon from "../asets/mode.svg"


const themeModes = ['dark', 'light']

export default function Header() {

    const { changeMode, mode } = useTheme() 
    const toggleMode = () => {
        changeMode(mode === 'dark' ? 'light' : 'dark')
    }

    console.log('Header selected mode :' + mode)

    return (
        <div className="theme-selector">
            <div className="mode-toggle">
                <img 
                onClick={ toggleMode }
                src={modeIcon} 
                alt="dark/light toggle icon"
                style={{filter: mode === 'dark' ? 'invert(100%)' : 'invert(20%)' }}
                />
            </div>

            <h1>Grocery Shopping</h1>
        </div>
  )
}
